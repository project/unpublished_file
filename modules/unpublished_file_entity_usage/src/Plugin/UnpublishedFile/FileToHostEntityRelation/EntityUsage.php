<?php

declare(strict_types=1);

namespace Drupal\unpublished_file_entity_usage\Plugin\UnpublishedFile\FileToHostEntityRelation;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\FileInterface;
use Drupal\unpublished_file\Attribute\FileToHostEntityRelation;
use Drupal\unpublished_file\FileToHostEntityRelationPluginBase;

/**
 * Detects relations between files and host entities from entity_usage records.
 *
 * This plugin returns the file host entities and the host entity files by
 * simply querying the entity_usage module's 'entity_usage.usage' service.
 */
#[FileToHostEntityRelation(
  id: 'entity_usage',
  label: new TranslatableMarkup('Entity Usage'),
  description: new TranslatableMarkup('Detects relations between file and host entity from entity_usage records.')
)]
class EntityUsage extends FileToHostEntityRelationPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getAccessibleHostEntities(FileInterface $file, AccountInterface $account): array {
    return $this->doGetAccessibleHostEntities($file, $account);
  }

  /**
   * {@inheritdoc}
   */
  public function hasAccessibleHostEntities(FileInterface $file, AccountInterface $account): bool {
    return !empty($this->doGetAccessibleHostEntities($file, $account, TRUE));
  }

  /**
   * Returns a list of accessible host entities given a file and a user.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account being checked.
   * @param bool $returnOnFirstFound
   *   (optional) Whether to return on first publicly available host entity
   *   found. Used as an optimization when the caller wants only to check if
   *   there is at least one published host entity. Defaults to FALSE.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   *   A list of host entities.
   */
  protected function doGetAccessibleHostEntities(FileInterface $file, AccountInterface $account, bool $returnOnFirstFound = FALSE): array {
    $hostEntities = [];

    foreach ($this->entityUsage->listSources($file) as $hostEntityTypeId => $usages) {
      $sourceEntityDefinition = $this->entityTypeManager->getDefinition($hostEntityTypeId);
      $hostEntityStorage = $this->entityTypeManager->getStorage($hostEntityTypeId);
      $ids = $hostEntityStorage->getQuery()
        ->accessCheck(FALSE)
        ->condition($sourceEntityDefinition->getKey('id'), array_keys($usages), 'IN')
        ->execute();
      if ($ids) {
        foreach ($hostEntityStorage->loadMultiple($ids) as $hostEntity) {
          if ($hostEntity->access('view', $account)) {
            $hostEntities[] = $hostEntity;
            if ($returnOnFirstFound) {
              // Optimization.
              return $hostEntities;
            }
          }
        }
      }
    }

    return $hostEntities;
  }

  /**
   * {@inheritdoc}
   */
  public function getFilesForHost(ContentEntityInterface $hostEntity): array {
    $files = [];
    foreach ($this->entityUsage->listTargets($hostEntity, $hostEntity->getRevisionId()) as $targetEntityTypeId => $usage) {
      if ($this->isFileEntityType($targetEntityTypeId)) {
        $files[$targetEntityTypeId] = array_keys($usage);
      }
    }
    return $files;
  }

}
