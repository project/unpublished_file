<?php

declare(strict_types=1);

namespace Drupal\Tests\unpublished_file_entity_usage\Functional;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\file\Functional\FileFieldCreationTrait;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\file\Entity\File;
use Drupal\node\Entity\NodeType;

/**
 * Tests for unpublished_file module.
 *
 * @group unpublished_file
 */
class UnpublishedFileEntityUsageTest extends BrowserTestBase {

  use FileFieldCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file',
    'image',
    'node',
    'unpublished_file_entity_usage',
  ];

  /**
   * Testing files.
   *
   * @var string[]
   */
  protected const TEST_FILES = [
    'field_file' => 'html-1.txt',
    'field_image' => 'image-1.png',
    'embed_file' => 'README.txt',
    'embed_image' => 'image-test.png',
  ];

  /**
   * Testing files.
   *
   * @var \Drupal\file\FileInterface[]
   */
  protected array $file = [];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $nodeType = NodeType::create(['type' => 'page']);
    $nodeType->save();
    node_add_body_field($nodeType);
    $this->createFileField('file', 'node', 'page', [], [
      'file_extensions' => 'txt',
    ]);
    FieldStorageConfig::create([
      'entity_type' => 'node',
      'field_name' => 'image',
      'type' => 'image',
    ])->save();
    FieldConfig::create([
      'entity_type' => 'node',
      'bundle' => 'page',
      'field_name' => 'image',
    ])->save();

    user_role_grant_permissions('anonymous', ['access content']);
    user_role_grant_permissions('authenticated', ['access content']);

    /** @var \Drupal\Core\File\FileSystemInterface $fileSystem */
    $fileSystem = \Drupal::service('file_system');
    $destinationDir = 'public://path/to';
    $sourceDir = DRUPAL_ROOT . '/core/tests/fixtures/files';
    $fileSystem->prepareDirectory($destinationDir, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    $this->assertDirectoryExists($destinationDir);
    foreach (static::TEST_FILES as $type => $file) {
      $fileUri = "$destinationDir/$file";
      $fileSystem->copy("$sourceDir/$file", $fileUri);
      $this->assertFileExists($fileUri);
      $this->file[$type] = File::create(['uri' => $fileUri, 'status' => TRUE]);
      $this->file[$type]->save();
    }
  }

  /**
   * Tests the module functionality.
   */
  public function testFileVisibility(): void {
    $regularUser = $this->createUser();
    $nodeOwner = $this->createUser(['view own unpublished content']);
    $accessBypasser = $this->createUser(['bypass node access']);

    /** @var \Drupal\node\NodeInterface[] $node */
    $node = [];
    for ($i = 0; $i < 2; $i++) {
      $node[$i] = $this->createNode([
        'type' => 'page',
        'title' => $this->randomString(),
        'file' => $this->file['field_file'],
        'image' => $this->file['field_image'],
        'uid' => $nodeOwner,
        // Node is not public.
        'status' => FALSE,
        'body' => [
          'value' => $this->buildBody(),
          'format' => 'basic_html',
        ],
      ]);
    }

    $this->assertFileIsNotAccessible();
    $this->assertFileIsNotAccessible($regularUser);
    $this->assertFileIsAccessible($nodeOwner);
    $this->assertFileIsAccessible($accessBypasser);

    // Publish the host entity.
    $node[0]->setPublished()->save();

    $this->assertFileIsAccessible();
    $this->assertFileIsAccessible($regularUser);
    $this->assertFileIsAccessible($nodeOwner);
    $this->assertFileIsAccessible($accessBypasser);

    // Unpublish again the host entity.
    $node[0]->setUnpublished()->save();

    $this->assertFileIsNotAccessible();
    $this->assertFileIsNotAccessible($regularUser);
    $this->assertFileIsAccessible($nodeOwner);
    $this->assertFileIsAccessible($accessBypasser);

    // Publish the 2nd node.
    $node[1]->setPublished()->save();

    $this->assertFileIsAccessible();
    $this->assertFileIsAccessible($regularUser);
    $this->assertFileIsAccessible($nodeOwner);
    $this->assertFileIsAccessible($accessBypasser);

    // Unpublish the 2nd node.
    $node[1]->setUnpublished()->save();

    $this->assertFileIsNotAccessible();
    $this->assertFileIsNotAccessible($regularUser);
    $this->assertFileIsAccessible($nodeOwner);
    $this->assertFileIsAccessible($accessBypasser);

    // Add a 3rd, published, node.
    $node[2] = $this->createNode([
      'type' => 'page',
      'title' => $this->randomString(),
      'file' => $this->file['field_file'],
      'image' => $this->file['field_image'],
      'uid' => $nodeOwner,
      'status' => TRUE,
      'body' => [
        'value' => $this->buildBody(),
        'format' => 'basic_html',
      ],
    ]);

    $this->assertFileIsAccessible();
    $this->assertFileIsAccessible($regularUser);
    $this->assertFileIsAccessible($nodeOwner);
    $this->assertFileIsAccessible($accessBypasser);

    // Delete the only published node.
    $node[2]->delete();

    $this->assertFileIsNotAccessible();
    $this->assertFileIsNotAccessible($regularUser);
    $this->assertFileIsAccessible($nodeOwner);
    $this->assertFileIsAccessible($accessBypasser);
  }

  /**
   * Asserts whether the file is accessible.
   *
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   The user account or NULL for anonymous.
   */
  protected function assertFileIsAccessible(?AccountInterface $account = NULL): void {
    $this->fileAccessAssertionHelper(200, $account);
  }

  /**
   * Asserts whether the file is not accessible.
   *
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   The user account or NULL for anonymous.
   */
  protected function assertFileIsNotAccessible(?AccountInterface $account = NULL): void {
    $this->fileAccessAssertionHelper(403, $account);
  }

  /**
   * Provides a helper method to check the access.
   *
   * @param int $code
   *   The response code expectation.
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   The user account or NULL for anonymous.
   */
  protected function fileAccessAssertionHelper(int $code, ?AccountInterface $account): void {
    \assert(in_array($code, [200, 403], TRUE));

    if ($account) {
      $this->drupalLogin($account);
    }

    foreach ($this->file as $file) {
      // Reload the file to get the latest changes.
      $file = File::load($file->id());

      // Perform a GET HTTP request to check file visibility.
      $this->drupalGet($file->createFileUrl(FALSE));

      $this->assertSession()->statusCodeEquals($code);
      $fileContents = trim(file_get_contents(DRUPAL_ROOT . "/core/tests/fixtures/files/{$file->getFilename()}"));
      if ($code === 200) {
        $this->assertSession()->responseContains($fileContents);
      }
      else {
        $this->assertSession()->responseNotContains($fileContents);
        $this->assertSession()->pageTextContains('Access denied');
      }
    }

    if ($account) {
      $this->drupalLogout();
    }
  }

  /**
   * Builds a testing body string.
   *
   * @return string
   *   Node body string.
   */
  protected function buildBody(): string {
    $fileUrlGenerator = $this->container->get('file_url_generator');
    return '<a data-entity-type="file" data-entity-uuid="' . $this->file['embed_file']->uuid() . '" href="' . $fileUrlGenerator->generateAbsoluteString($this->file['embed_file']->getFileUri()) . '">README</a> ' .
      $this->randomString(20) .
      ' <img data-entity-type="' . $this->file['embed_image']->getEntityTypeId() . '" data-entity-uuid="' . $this->file['embed_image']->uuid() . '" src="' . $fileUrlGenerator->generateAbsoluteString($this->file['embed_image']->getFileUri()) . '"> ' .
      $this->randomString(20);
  }

}
