<?php

declare(strict_types=1);

namespace Drupal\unpublished_file;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AnonymousUserSession;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\file\FileInterface;
use Drupal\file\FileRepositoryInterface;

/**
 * Default implementation for 'unpublished_file' service.
 */
class UnpublishedFile implements UnpublishedFileInterface {

  /**
   * The anonymous user.
   */
  protected AccountInterface $anonymous;

  public function __construct(
    protected readonly FileToHostEntityRelationPluginManager $manager,
    protected readonly ConfigFactoryInterface $configFactory,
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    protected readonly FileSystemInterface $fileSystem,
    protected readonly FileRepositoryInterface $fileRepository,
  ) {
    $this->anonymous = new AnonymousUserSession();
  }

  /**
   * {@inheritdoc}
   */
  public function setFilesVisibility(EntityInterface $hostEntity): void {
    if (!$hostEntity instanceof ContentEntityInterface) {
      return;
    }

    $originalHostEntity = !empty($hostEntity->original) ? $hostEntity->original : NULL;
    if ($originalHostEntity && $originalHostEntity->access('view', $this->anonymous) === $hostEntity->access('view', $this->anonymous)) {
      // The host entity is updating but the visibility for the anonymous user
      // remains unchanged. Nothing to do.
      return;
    }

    foreach ($this->getHostEntityFiles($hostEntity) as $fileEntityTypeId => $fileIds) {
      // Different plugins may return same files.
      $fileIds = array_unique($fileIds);
      $fileStorage = $this->entityTypeManager->getStorage($fileEntityTypeId);
      foreach ($fileStorage->loadMultiple($fileIds) as $file) {
        assert($file instanceof FileInterface);
        $hasAccessibleHostUsage = $this->hasAnonymousAccessibleUsage($file);

        // @todo What about `$settings['file_additional_public_schemes']`?
        if (!$hasAccessibleHostUsage && str_starts_with($file->getFileUri(), 'public://')) {
          $this->moveFile($file, static::PRIVATE_LOCATION . StreamWrapperManager::getTarget($file->getFileUri()));
        }
        elseif ($hasAccessibleHostUsage && str_starts_with($file->getFileUri(), static::PRIVATE_LOCATION)) {
          [, $path] = explode('/', StreamWrapperManager::getTarget($file->getFileUri()), 2);
          $this->moveFile($file, "public://$path");
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getHostEntityFiles(ContentEntityInterface $hostEntity): array {
    // Check first if the files were not computed already in the same request.
    // This happens usually when deleting the host entity.
    // @see unpublished_file_entity_predelete()
    if (isset($hostEntity->unpublished_file['files']) && is_array($hostEntity->unpublished_file['files'])) {
      return $hostEntity->unpublished_file['files'];
    }

    // Collect files from plugins.
    $files = [];
    foreach ($this->manager->getDefinitions() as $pluginId => $pluginDefinition) {
      $plugin = $this->manager->createInstance($pluginId);
      assert($plugin instanceof FileToHostEntityRelationPluginInterface);
      $files = array_merge_recursive($files, $plugin->getFilesForHost($hostEntity));
    }

    // Different plugins might return same files.
    return array_map('array_unique', $files);
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessibleHostEntities(FileInterface $file, AccountInterface $account): array {
    $hostEntities = [];

    // @todo Not sure excluding temporary files is correct.
    if (!$file->isPermanent()) {
      // Don't mess with temporary files.
      return $hostEntities;
    }

    foreach ($this->manager->getDefinitions() as $pluginId => $pluginDefinition) {
      $plugin = $this->manager->createInstance($pluginId);
      assert($plugin instanceof FileToHostEntityRelationPluginInterface);
      $hostEntities += $plugin->getAccessibleHostEntities($file, $account);
    }

    return $hostEntities;
  }

  /**
   * Checks whether a file has at least one anonymous-accessible usage.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file entity to be checked.
   *
   * @return bool
   *   Whether a file has at least one anonymous-accessible usage.
   */
  protected function hasAnonymousAccessibleUsage(FileInterface $file): bool {
    foreach ($this->manager->getDefinitions() as $pluginId => $pluginDefinition) {
      $plugin = $this->manager->createInstance($pluginId);
      assert($plugin instanceof FileToHostEntityRelationPluginInterface);
      if ($plugin->hasAccessibleHostEntities($file, $this->anonymous)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Moves a given file to a given destination.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file entity.
   * @param string $destinationUri
   *   The destination URI.
   */
  protected function moveFile(FileInterface $file, string $destinationUri): void {
    $destinationDir = $this->fileSystem->dirname($destinationUri);
    if (!$this->fileSystem->prepareDirectory($destinationDir, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
      throw new \RuntimeException("Directory $destinationDir cannot be created or it's not writable.");
    }
    $this->fileRepository->move($file, $destinationUri);
  }

}
