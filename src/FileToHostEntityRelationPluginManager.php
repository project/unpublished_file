<?php

declare(strict_types=1);

namespace Drupal\unpublished_file;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\unpublished_file\Attribute\FileToHostEntityRelation;

/**
 * Plugin manager for FileSource plugins.
 */
class FileToHostEntityRelationPluginManager extends DefaultPluginManager {

  public function __construct(\Traversable $namespaces, CacheBackendInterface $cacheBackend, ModuleHandlerInterface $moduleHandler) {
    parent::__construct('Plugin/UnpublishedFile/FileToHostEntityRelation', $namespaces, $moduleHandler, FileToHostEntityRelationPluginInterface::class, FileToHostEntityRelation::class);
    $this->alterInfo('unpublished_file_to_host_info');
    $this->setCacheBackend($cacheBackend, 'unpublished_to_host');
  }

}
