<?php

namespace Drupal\unpublished_file;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\file\FileInterface;

/**
 * Interface for FileSource plugins.
 *
 * Such plugins are able to return the host entities of a file entity using a
 * specific method.
 *
 * It's the plugin business to determine the way to reach the host entity of a
 * given file or get all the file entities of a given host entity. For instance,
 * a certain plugin might use the Entity Usage data which computes and stores
 * relations between entities.
 */
interface FileToHostEntityRelationPluginInterface extends PluginInspectionInterface {

  /**
   * Returns a list of accessible host entities for a given file and user.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account being checked.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   *   A list of host entities.
   */
  public function getAccessibleHostEntities(FileInterface $file, AccountInterface $account): array;

  /**
   * Checks whether the given file has accessible host entities.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account being checked.
   *
   * @return bool
   *   Whether the given file has accessible host entities.
   */
  public function hasAccessibleHostEntities(FileInterface $file, AccountInterface $account): bool;

  /**
   * Returns a list of file entities referred by a host entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $hostEntity
   *   The host entity.
   *
   * @return array[]
   *   An associative array having the file entity type ID as keys and a list of
   *   file entity IDs as values.
   */
  public function getFilesForHost(ContentEntityInterface $hostEntity): array;

}
