<?php

namespace Drupal\unpublished_file;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\file\FileInterface;

/**
 * Interface of 'unpublished_file' service.
 */
interface UnpublishedFileInterface {

  /**
   * Location of files in the private filesystem.
   */
  public const PRIVATE_LOCATION = 'private://unpublished-file/';

  /**
   * Moves the files from public to private file system or vice versa.
   *
   * @param \Drupal\Core\Entity\EntityInterface $hostEntity
   *   The host entity.
   */
  public function setFilesVisibility(EntityInterface $hostEntity): void;

  /**
   * Returns a list of files belonging to a host entity.
   *
   * The files are collected by iterating over all plugins.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $hostEntity
   *   The host entity.
   *
   * @return array
   *   An associative array having the file entity type ID as keys and a list of
   *   file entity IDs as values.
   */
  public function getHostEntityFiles(ContentEntityInterface $hostEntity): array;

  /**
   * Returns a list of accessible host entities for a given file and user.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account being checked.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   *   A list of host entities.
   */
  public function getAccessibleHostEntities(FileInterface $file, AccountInterface $account): array;

}
