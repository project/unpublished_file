<?php

namespace Drupal\unpublished_file\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines an attribute for FileHostEntity plugins discovery.
 *
 * Plugin Namespace: Plugin\UnpublishedFile\FileToHostEntityRelation.
 *
 * @see plugin_api
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class FileToHostEntityRelation extends Plugin {

  /**
   * Constructs an EntityReferenceSelection attribute.
   *
   * @param string $id
   *   The plugin ID.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $label
   *   The human-readable name of the selection plugin.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|null $description
   *   (optional) The plugin description.
   */
  public function __construct(
    public readonly string $id,
    public readonly TranslatableMarkup $label,
    public readonly ?TranslatableMarkup $description = NULL,
  ) {}

}
