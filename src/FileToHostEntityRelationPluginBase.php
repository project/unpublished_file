<?php

declare(strict_types=1);

namespace Drupal\unpublished_file;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\entity_usage\EntityUsageInterface;
use Drupal\file\FileInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for accessible usages plugins.
 */
abstract class FileToHostEntityRelationPluginBase extends PluginBase implements FileToHostEntityRelationPluginInterface, ContainerFactoryPluginInterface {

  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected readonly EntityUsageInterface $entityUsage,
    protected readonly EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_usage.usage'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Checks whether a given entity type ID designates a file entity.
   *
   * Check also potential entity types implementing \Drupal\file\FileInterface,
   * other than 'file'.
   *
   * @param string $entityTypeId
   *   The entity type ID to be checked.
   *
   * @return bool
   *   Whether a given entity type ID designates a file entity.
   */
  protected function isFileEntityType(string $entityTypeId): bool {
    return $this->entityTypeManager->getDefinition($entityTypeId)->entityClassImplements(FileInterface::class);
  }

}
