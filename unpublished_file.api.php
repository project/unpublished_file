<?php

/**
 * @file
 * Hooks for the unpublished_file module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Allows modules to alter the FileSource plugin definitions.
 *
 * Modules implementing this hook should return TRUE if the operation should
 * be blocked. Any other return value will be disregarded and the register
 * written to the database.
 *
 * @param array $info
 *   Array of plugin information exposed by hook page section plugins, altered
 *   by reference.
 *
 * @see \Drupal\unpublished_file\FileToHostEntityRelationPluginManager
 * @see \Drupal\unpublished_file\FileToHostEntityRelationPluginInterface
 */
function hook_unpublished_file_source_info_alter(array &$info): void {
  $info['entity_usage']['title'] = t('Simple relation');
}

/**
 * @} End of "addtogroup hooks".
 */
