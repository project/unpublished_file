# Unpublished File

Keeps public files of a publicly inaccessible entity in the private filesystem until the entity becomes available for public. Entity files are all files referred by the entity, either via entity reference fields or uploaded and embedded in text blobs.

## Terminology

* **File**: A file entity attached to a host entity.
* **Host entity**: A content entity that refers a file entity directly, or via some different entities which are located in the middle. The reference could be an entity reference field, a file uploaded and linked via CKEditor and so on. For instance, a node might expose a paragraph field which contains a text paragraph which embeds a file media entity. In this case the host entity is the node and paragraph and media are _entities in the middle_.

## How it works?

In order to be able to move a file from public to private filesystem and vice versa, the module should be able to determine which is the host entity for a certain file and determine its visibility for anonymous users. This is done via the `unpublished_file` service. The service interrogates all `FileToHostEntityRelation` plugins and collects all the host entities. If at least one is publicly accessible, the file is move in the public filesystem. If none is publicly accessible, the file is move in the private filesystem.

The main module, `unpublished_file`, offers the `FileToHostEntityRelation` plugin type. This plugin type knows how to determine the relation between a file and its host entity. By default, the module only provides the plugin discovery attributes, the manager and the interface, but no plugin class. But there is the [unpublished_file_entity_usage](modules/unpublished_file_entity_usage) submodule that ships a plugin which computes such relation by extracting data from the Entity Usage module.

Third-party modules can provide their own plugins using different methods. 

## Configuration

This section covers the configuration when using the [unpublished_file_entity_usage](modules/unpublished_file_entity_usage) submodule.

Configure [Entity Usage](https://www.drupal.org/project/entity_usage). Note that this is critical for the module to work, and it's crucial to understand how [Entity Usage](https://www.drupal.org/project/entity_usage) works and how to track the relation between host entities, such as _Content_ (`node`), and _File_ (`file`) entities.

* Visit `/admin/config/entity-usage/settings`.
* Carefully check the entity types under _Enabled source entity types_ section. The files visibility depend on the visibility of their source entities (the entity that refers the file). Note that the reference is not limited to entity reference fields, a reference to a file can be also an image uploaded and embedded in a text field, using CKEditor.
* Select the file entity types, under _Enabled target entity types_ section. In most of the cases it's only the _File_ (`file`) that should be selected unless you have other entity types, implementing `\Drupal\file\FileInterface`.
* Select the tracking plugins under _Enabled tracking plugins_ section. These plugins knowing how determine the relation between a source entity (e.g., `node`) and the target entity (`file`). For instance, if your node-type has a file or an image field, you should check the _Entity Reference_ plugin. If the node body allows also embedding images and files using data attributes, such as `drupal-entity-type="file"` and `drupal-entity-uuid="...""`, you should check one of _CKEditor Image_ or/and _LinkIt_, accordingly.

## Contribute

[DDEV](https://ddev.com), a Docker-based PHP development tool for a streamlined and unified development process, is the recommended tool for contributing to the module. The [DDEV Drupal Contrib](https://github.com/ddev/ddev-drupal-contrib) addon makes it easy to develop a Drupal module by offering the tools to set up and test the module.

### Install DDEV

* Install a Docker provider by following DDEV [Docker Installation](https://ddev.readthedocs.io/en/stable/users/install/docker-installation/) instructions for your Operating System.
* [Install DDEV](https://ddev.readthedocs.io/en/stable/users/install/ddev-installation/), use the documentation that best fits your OS.
* DDEV is used mostly via CLI commands. [Configure shell completion & autocomplete](https://ddev.readthedocs.io/en/stable/users/install/shell-completion/) according to your environment.
* Configure your IDE to take advantage of the DDEV features. This is a critical step to be able to test and debug your module. Remember, the website runs inside Docker, so pay attention to these configurations:
  - [PhpStorm Setup](https://ddev.readthedocs.io/en/stable/users/install/phpstorm/)
  - [Configure](https://ddev.readthedocs.io/en/stable/users/debugging-profiling/step-debugging/) PhpStorm and VS Code for step debugging.
  - Profiling with [xhprof](https://ddev.readthedocs.io/en/stable/users/debugging-profiling/xhprof-profiling/), [Xdebug](https://ddev.readthedocs.io/en/stable/users/debugging-profiling/xdebug-profiling/) and [Blackfire](https://ddev.readthedocs.io/en/stable/users/debugging-profiling/blackfire-profiling/).

### Checkout the module

Normally, you check out the code form an [issue fork](https://www.drupal.org/docs/develop/git/using-gitlab-to-contribute-to-drupal/creating-issue-forks):

```shell
git clone git@git.drupal.org:issue/unpublished_file-[issue number].git
cd unpublished_file-[issue number]
```

### Start DDEV

Inside the cloned project run:

```shell
ddev start
```

This command will fire up the Docker containers and add all configurations.

### Install dependencies

```shell
ddev poser
```

This will install the PHP dependencies. Note that this is a replacement for Composer _install_ command that knows how to bundle together Drupal core and the module. Read more about this command at https://github.com/ddev/ddev-drupal-contrib?tab=readme-ov-file#commands

```shell
ddev symlink-project
```
This symlinks the module inside `web/modules/custom`. Read more about this command at https://github.com/ddev/ddev-drupal-contrib?tab=readme-ov-file#commands. Note that as soon as `vendor/autoload.php` has been generated, this command runs automatically on every `ddev start`.

This command should also be run when adding new directories or files to the root of the module.

### Install Drupal

```shell
ddev install
```

This will install Drupal and will enable the module.

### Changing the Drupal core version

* Create a file `.ddev/config.local.yaml`
* Set the desired Drupal core version. E.g.,
  ```yaml
  web_environment:
    - DRUPAL_CORE=^11
  ```
* Run `ddev restart`

### Run tests

* `ddev phpunit`: run PHPUnit tests
* `ddev phpcs`: run PHP coding standards checks
* `ddev phpcbf`: fix coding standards findings
* `ddev phpstan`: run PHP static analysis
